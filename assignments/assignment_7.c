/*  
Implement doubly linked list of bank accounts. Each account has information including id, type,
balance and account holder. The account holder details include name, address & contact details.
Write a menu-driven program to implement add first, add last, display all (forward), display all
(backword), find by account id, find by account holder name, delete all functionalities.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum options
{
    Exit,
    add_first,
    add_last,
    forward_display,
    backward_display,
    find_acc_id,
    find_acc_name
};

typedef struct accholder
{
    char name[15];
    char address[20];
    char contact[12];
} Accholder_t;

typedef struct account
{
    int id;
    char type[12];
    double balance;
    Accholder_t acc_holder;
    struct account *next;
    struct account *prev;
} Account_t;

Account_t *head = NULL;
Account_t *create_account();
void add_at_first();
void add_at_last();
void display_forward();
void display_backward();
void find_acc_by_id(int id);
void find_acc_by_name(char name[]);
void free_all();

int main()
{
    int choice, id;
    char name[15];
    do
    {
        printf("Enter choice : ");
        printf("0.Exit   1.Add first   2.Add last   3.Display forward   4.Display bacword   5.Find by id   6.Find by name\n");
        scanf("%d", &choice);

        switch (choice)
        {
        case Exit:
            free_all();
            break;

        case add_first:
            add_at_first();
            break;

        case add_last:
            add_at_last();
            break;

        case forward_display:
            display_forward();
            break;

        case backward_display:
            display_backward();
            break;

        case find_acc_id:
            printf("Enter ID : ");
            scanf("%d", &id);
            find_acc_by_id(id);

            break;

        case find_acc_name:
            printf("Enter Name : ");
            scanf("%s", name);
            find_acc_by_name(name);
            break;
        }
    } while (choice != Exit);
    return 0;
}

Account_t *create_account()
{
    Account_t *temp = (Account_t *)malloc(sizeof(Account_t));
    if (temp == NULL)
    {
        perror("Memory is not allocated.\n");
        exit(1);
    }
    printf("ID : ");
    scanf("%d", &temp->id);
    printf("Type : ");
    scanf("%s", temp->type);
    printf("Balance : ");
    scanf("%lf", &temp->balance);
    printf("Name : ");
    scanf("%s", temp->acc_holder.name);
    printf("Address : ");
    scanf("%s", temp->acc_holder.address);
    printf("Contact : ");
    scanf("%s", temp->acc_holder.contact);
    temp->next = NULL;
    temp->prev = NULL;
    return temp;
}

void add_at_first()
{
    Account_t *New = create_account();

    if (head == NULL)
        head = New;
    else
    {
        New->next = head;
        head->prev = New;
        head = New;
    }
}

void add_at_last()
{
    Account_t *New = create_account();

    if (head == NULL)
        head = New;
    else
    {
        Account_t *temp = head;
        while (temp->next != NULL)
            temp = temp->next;

        temp->next = New;
        New->prev = temp;
    }
}

void display_forward()
{
    if (head != NULL)
    {
        Account_t *temp = head;
        while (temp != NULL)
        {
            printf("%d, %s, %.2lf, %s, %s, %s\n", temp->id, temp->type, temp->balance, temp->acc_holder.name, temp->acc_holder.address, temp->acc_holder.contact);
            temp = temp->next;
        }
    }
    else
        printf("No accounts in the list.\n");
}

void display_backward()
{
    if (head != NULL)
    {
        Account_t *temp = head;
        while (temp->next != NULL)
            temp = temp->next;

        while (temp != NULL)
        {
            printf("%d, %s, %.2lf, %s, %s, %s\n", temp->id, temp->type, temp->balance, temp->acc_holder.name, temp->acc_holder.address, temp->acc_holder.contact);
            temp = temp->prev;
        }
    }
    else
        printf("No accounts in the list.\n");
}

void find_acc_by_id(int id)
{
    Account_t *temp = head;
    int flag = 0;
    if (head != NULL)
    {
        while (temp != NULL)
        {
            if (id == temp->id)
            {
                printf("Account found with the id : %d and details are\n", id);
                printf("%d, %s, %.2lf, %s, %s, %s\n", temp->id, temp->type, temp->balance, temp->acc_holder.name, temp->acc_holder.address, temp->acc_holder.contact);
                flag = 1;
                break;
            }
            temp = temp->next;
        }
        if (flag == 0)
            printf("Not found account with the ID : %d\n", id);
    }
    else
        printf("List is empty.\n");
}

void find_acc_by_name(char name[])
{
    Account_t *temp = head;
    int flag = 0;
    if (head != NULL)
    {
        while (temp != NULL)
        {
            if (strcmp(temp->acc_holder.name, name) == 0)
            {
                printf("Account found with the name : %s and details are\n", name);
                 printf("%d, %s, %.2lf, %s, %s, %s\n", temp->id, temp->type, temp->balance, temp->acc_holder.name, temp->acc_holder.address, temp->acc_holder.contact);
                flag = 1;
                break;
            }
            else
                temp = temp->next;
        }
        if (flag == 0)
            printf("Not found account with name : %s.\n", name);
    }
    else
        printf("List is empty.\n");
}

void free_all()
{
    if (head != NULL)
    {
        if (head->next == NULL)
        {
            free(head);
            head = NULL;
            printf("All nodes are deleted.\n");
        }
        else
        {
            Account_t *temp = head;
            head = temp->next;
            free(temp);
            temp = NULL;
        }
    }
    else
        printf("List is empty.\n");
}