/* Maintain an array of ten positive numbers. Initially all elements are set to zero 
(indicating array is empty). Write a menu driven program to perform operations like add number,
delete number, find maximum number (along with its index), find minimum number (along with its index)
and sum of numbers. While adding number display available indexes and user can select any of them. If no 
index is free, display appropriate message. Also, while deleting number display available indexes
along with values and user can clear value there (set to zero).*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
    int choice;
    int a[10]={0};
    do{
        printf(" \n0.Exit\n1.add\n2.Delete\n3.Max\n4.Min\n5.sum of all array\nEnter choice:");
        scanf("%d",&choice);
        switch (choice){
            case 1 : add(a);
                     break;
            case 2 : Delete(a);
                     break;
            case 3 : Max(a);
                     break;
           case 4 : Min(a);
                     break;
            case 5 : sum_of_all_array(a);
                     break;
            
        }

    }while(choice!=0);

}

int add(int a[]){
    int count=0;
    for(int i=0;i<10;i++){
        if(a[i]==0){
           count++;
           printf("%d\t",i);

        }
    }
    printf("\n%d",count);
    int index,num;
    if(count>0){
        printf("\nEnter position where you want to add :");
        scanf("%d",&index);
        printf("Enter number =");
        scanf("%d",&a[index]);
    }
    else{
        printf("\narray is full.");
    }
}

int Delete(int a[]){
    int index;
    for(int i=0;i<10;i++){
        printf("%d\t",i);
        printf("%d\n",a[i]);
    }
    printf("Enter index no:");
    scanf("%d",&index);
    memset(&a[index], 0, sizeof(int));

}

void Max(int a[]){
    int max=a[0];
    for(int i=1;i<10;i++){
        
        if(a[i]>max){
            max=a[i];
            
        }
    }
    printf("MAX =%d",max);

}
void Min(int a[]){
    int min=a[0];
    for(int i=1;i<10;i++){
        
        if(a[i]<min){
            min=a[i];
            
        }
    }
    printf("MIN =%d",min);

    
}
void sum_of_all_array(int a[]){
    int sum=0;
    for(int i=0;i<10;i++){
        sum=sum+a[i];
    }
    printf("SUM=%d",sum);
}