//Simulate string library functions strcpy, strcat, strcmp and strrev.
#include <stdio.h>
#include <string.h>

enum option
{
    Exit,
    strCpy,
    strCat,
    strRev,
    strCmp
};

int main()
{
    char dest[20];
    char src[20];
    int choice;

    do
    {
        printf("\nEnter choice 0.Exit 1.String copy 2.String concat 3.String reverse 4.String compare \n");
        scanf("%d", &choice);
        switch (choice)
        {
        case strCpy:
            printf("Enter the string : ");
            scanf("%s", dest);
            strcpy(src, dest);
            printf("destination string : %s\n", dest);
            printf("source string : %s\n", src);
            break;

        case strCat:
            printf("Enter the string 1 : ");
            scanf("%s", src);
            printf("Enter the string 2 : ");
            scanf("%s", dest);
            strcat(src, dest);
            printf("Resultant string is : %s\n", src);
            break;

        case strRev:
            printf("Enter the string : ");
            scanf("%s", dest);
            strrev(dest);
            printf("Reverse string is : %s\n", dest);
            break;

        case strCmp:
            printf("Enter the string 1 : ");
            scanf("%s", src);
            printf("Enter the string 2 : ");
            scanf("%s", dest);
            int val = strcmp(src, dest);
            if (val == 0)
                printf("Both strings are equal\n");

            else
                printf("Strings are not equal\n");

            break;
        }
        printf("-----------------------------------------------------------------------------------\n");
    } while (choice != Exit);
    return 0;
}
