/*An inventory management system needs to manage data of the items into binary file. Each item has id, name, 
price and quantity. Write a menu-driven program to implement add, find, display all, edit and delete 
operations from the items file. Order id (int) should be generated automatically and must be unique.*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define ITEMS_DB  "items.db"


typedef struct items
{
    int id;
    char name[80];
    double price;
    int quantity;
}items_t;

void add_items(items_t *i){
    printf("Name :");
    scanf("%s",i->name);
    printf("price:");
    scanf("%lf",&i->price);
    printf("quantity:");
    scanf("%d",&i->quantity);

}

void items_display(items_t *i) {
	printf("id:%d, name:%s, price:%.2lf, quantity:%d\n", i->id, i->name,i->price,i->quantity );
}

int get_next_item_id(){
	FILE *fp;
	int max = 0;
	int size = sizeof(items_t);
	items_t u;
	// open the file
	fp = fopen(ITEMS_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
		
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;

}

void add(){
    items_t i;
	add_items(&i);
	i.id = get_next_item_id();
    FILE *fp;
    fp=fopen(ITEMS_DB,"ab");
    if(fp==NULL){
        perror("cannot open file.");
    }
    fwrite(&i,sizeof(items_t),1,fp);
    printf("items added into file");
    fclose(fp);

}

void find_item(char name[]){
	FILE *fp;
	items_t b;
	int found = 0;
	fp = fopen(ITEMS_DB,"rb");
	if(fp==NULL){
		 	perror("cannot open file");
			return;
	}
	while(fread(&b,sizeof(items_t),1,fp)>0){
			if(strstr(b.name,name)!=NULL){
					found = 1;
					items_display(&b);
			}
	}
	fclose(fp);
	if(!found){
		printf("unanle to find your requirement\n sorry for your inconvinence");
	}
}


void display_all(){
    FILE *fp;
    items_t i;
    fp=fopen(ITEMS_DB,"rb");
    if(fp==NULL){
        perror("file cannot open");
    }
    while(fread(&i,sizeof(items_t),1,fp)>0){
        items_display(&i);
    }
    fclose(fp);
}
void edit_item(){
    int id, found = 0;
	FILE *fp;
	items_t i;
	// input book id from user.
	printf("enter item id: ");
	scanf("%d", &id);
	
	fp = fopen(ITEMS_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open books file");
	}
	
	while(fread(&i, sizeof(items_t), 1, fp) > 0) {
		if(id == i.id) {
			found = 1;
			break;
		}
	}

	if(found) {
		
		long size = sizeof(items_t);
		items_t nb;
		add_items(&nb);
		nb.id = i.id;
		fseek(fp, -size, SEEK_CUR);
		
		fwrite(&nb, sizeof(items_t), 1, fp);
		printf("item updated.\n");
	}
	else 
		
		printf("item not found.\n");
	
	fclose(fp);
}

void delete_item(int id)
{
    FILE *fp, *fs;
    items_t i;
    fp = fopen(ITEMS_DB, "rb+");
    fs = fopen("temp.db", "wb+");
    if (fp == NULL || fs == NULL)
    {
        perror("File cannot open.\n");
        return;
    }


    while (fread(&i, sizeof(items_t), 1, fp) > 0)
    {
        if (i.id != id)
        {
            fwrite(&i, sizeof(items_t), 1, fs);
        }
    }
    fclose(fp);
    fclose(fs);
    remove(ITEMS_DB);
    rename("temp.db", ITEMS_DB);
}


void main(){
    int choice,id;

    char b[20];
    do{
        printf(" \n0.Exit\n1.add\n2.find\n3.dispaly_all\n4.edit\n5.delete\nEnter choice:");
        scanf("%d",&choice);
        switch (choice){
            case 1 : add();
                     break;
            case 2 : printf("Enter book name you want to find :");
					scanf("%s",&b);
                    find_item(b);
                     break;
            case 3 : display_all();
                     break;
            case 4 : edit_item();
                     break;
            case 5 : printf("Enter ID ");
                     scanf("%d", &id);
                     delete_item(id);
                     break;
            
        }

    }while(choice!=0);
}


