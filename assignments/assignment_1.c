//Input a string from user on command line. String may have multiple commas 
//e.g. "Welcome,to,Sunbeam,CDAC,Diploma,Course". Print each word individually.
// Hint: use strtok() function.
#include <stdio.h>
#include <string.h>

int main(void){
    char a[100];
    char *token;
    printf("Enter string :");
    scanf("%s",&a);

    token=strtok(a,",");
    while(token != NULL){
        printf("%s\n",token);

        token=strtok(NULL, ",");
    }
    return(0);
 
}
